import React, {Component} from 'react';
import './pollItem.css'

class PollItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0
    }
  }
  render() {
    return (
      <div className={'pollItem'}>
        <p onClick={() => {
          const clicks = this.state
          clicks.value++
          this.setState(clicks)
        }}>{this.props.text} - {this.state.value}</p>
      </div>
    );
  }
}

export default PollItem;