import React, {Component} from 'react';
import PollItem from "../PollItem/pollItem";
import './poll.css'

class Poll extends Component {
  render() {
    return (
      <div className={'pollDiv'}>
        <p>{this.props.text}</p>
        <PollItem text={'Yes'}/>
        <PollItem text={'No'}/>
        <PollItem text={'Maybe'}/>
        <PollItem text={'IDK'}/>
        <PollItem text={'))))'}/>
      </div>
    );
  }
}

export default Poll;