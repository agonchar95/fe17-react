import React, {Component} from 'react';
import './App.css';
import Auth from "./components/Auth/Auth";
import Main from "./pages/Main/Main";

class App extends Component{
  state = {
    isAuth: false
  };
  setisAuth() {
    this.setState({
      isAuth: true
    })
  }

  render() {
    const {isAuth} = this.state;

    return (
      <div className="App">
        {
          isAuth
          ? <Main/>
          : <Auth setIsAuthHandler={() => this.setisAuth()}/>
        }
      </div>
    );
  }
}

export default App;
