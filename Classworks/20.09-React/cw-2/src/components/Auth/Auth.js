import React, {Component} from 'react';

class Auth extends Component {
  state = {
    email: '',
    password: ''
  }
  async handleSubmit(event) {
   event.preventDefault()
    const response = await fetch('./auth.json').then(r => r.json())
    const {email, password} = this.state
    if (response.email === email && response.password === password) {
      this.props.setIsAuthHandler()
    }
  }
  handleInputChange(inputType, inputValue) {
    this.setState({
      [inputType]: inputValue
    })
  }
  render() {
    return (
      <form onSubmit={(event) => this.handleSubmit(event)}>
        <input type="email" placeholder="Enter email" onChange={(event) => this.handleInputChange('email', event.target.value)}/>
        <input type="password" placeholder="Enter password" onChange={(event) => this.handleInputChange('password', event.target.value)}/>
        <button type="submit">log in</button>
      </form>
    );
  }
}

export default Auth;