import React, {Component} from 'react';
import './App.css';
import Auth from "./components/Auth/Auth";
import Main from "./pages/Main/Main";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";

class App extends Component{
  state = {
    isAuth: false
  };

  setIsAuth(val) {
    this.setState({
      isAuth: val
    })
  }

  render() {
    const {isAuth} = this.state;

    return (
      <div className="App">
        <Header isAuth={isAuth} handleLogout={() => this.setIsAuth(false)}/>
        {
          isAuth
          ? <Main/>
          : <Auth setIsAuthHandler={() => this.setIsAuth(true)}/>
        }
        <Footer/>
      </div>
    );
  }
}

export default App;
