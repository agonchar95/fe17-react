import React, {Component} from 'react';
import EmailItem from "../../components/EmailItem/EmailItem";

class Main extends Component {
  state = {
    items: []
  }
  componentDidMount() {
    fetch('./mailStore.json')
      .then(r => r.json())
      .then(mails => this.setState({
        items: mails
      }))

  }

  render() {
    const {items} = this.state
    const emails = items.map(el => <EmailItem email={el} key={el.id} />)
    return (
      <div className="container">
        {emails}
      </div>
    );
  }
}

export default Main;