import React, {PureComponent} from 'react';
import PollItem from "./PollItem";

class Poll extends PureComponent {
  // constructor(props) {
  //   super(props);
  // const newState = {};
  // const {items} = this.props;
  //
  // items.forEach(itemName => newState[itemName] = 0);
  //
  // this.state = newState;
  // }

  // handleVote (event, votedName) {
  //   this.setState({
  //     [votedName]: this.state[votedName] + 1
  //   })
  // }

  render() {
    const {title, items} = this.props;
    const pollItems = items.map(
      (itemName, index) => (
        <PollItem name={itemName}
                  // handleClick={e => this.handleVote(e, itemName)}
                  // count={this.state[itemName]}
                  key={index}/>
      )
    );

    return (
      <div>
        <h2>{title}</h2>
        {pollItems}
      </div>
    );
  }
}

export default Poll;