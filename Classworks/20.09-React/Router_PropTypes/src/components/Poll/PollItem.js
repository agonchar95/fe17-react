import React, {Component} from 'react';

class PollItem extends Component {
  state = {
    count: 0
  };

  handleVote () {
    const {count} = this.state;

    this.setState({
      count: count + 1
    })
  }

  render() {
    const {name} = this.props;
    const {count} = this.state;
    return (
      <div onClick={() => this.handleVote()}>
        <p>{name}</p>
        <p>{count}</p>
      </div>
    );
  }
}

export default PollItem;