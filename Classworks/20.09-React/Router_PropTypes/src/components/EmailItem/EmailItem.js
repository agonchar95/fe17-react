import React, {Component} from 'react';
import PropTypes from 'prop-types'

class EmailItem extends Component {
  static propTypes = {
    email: PropTypes.object
  }
  render() {
    const {email: {from, to, text}} = this.props
    return (
      <div>
        <p>From: {from}</p>
        <p>To: {to}</p>
        <p>Message: {text}</p>
      </div>
    );
  }
}

export default EmailItem;