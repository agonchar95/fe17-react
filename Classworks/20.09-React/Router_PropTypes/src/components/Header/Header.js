import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Logo from '../../logo.svg';
import './Header.scss'

class Header extends Component {
  static propTypes = {
    isAuth: PropTypes.bool,
    handleLogout: PropTypes.func
  };

  render() {
    const {handleLogout, isAuth} = this.props;
    return (
      <header className={'page-top'}>
        <a href="#" className={'page-top__img'}>
          <img src={Logo} alt="react logo"/>
        </a>
        {
          isAuth && <button className={'page-top__btn'} onClick={handleLogout}>logout</button>
        }
      </header>
    );
  }
}

export default Header;