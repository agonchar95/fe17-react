import React, {Component} from 'react';
import './Auth.scss';
import PropTypes from 'prop-types';

class Auth extends Component {
  static propTypes = {
    setIsAuthHandler: PropTypes.func
  };

  state = {
    email: '',
    password: ''
  };

  async handleSubmit(event) {
    event.preventDefault();
    const response = await fetch('./auth.json').then(r => r.json());
    const {email, password} = this.state;
    const {setIsAuthHandler} = this.props;

    if (response.email === email
      && response.password === password) {
      setIsAuthHandler()
    }
  }

  handleInputChange(inputType, inputValue) {
    this.setState({
      [inputType]: inputValue
    })
  }

  render() {
    return (
      <form className={'auth'} onSubmit={(e) => this.handleSubmit(e)}>
        <input className={'auth__field'}
               type="email"
               placeholder="Enter email"
               onChange={event => this.handleInputChange('email', event.target.value)}/>
        <input className={'auth__field'}
               type="password"
               placeholder="Enter password"
               onChange={event => this.handleInputChange('password', event.target.value)}/>
        <button className={'auth__btn'} type="submit">log in</button>
      </form>
    );
  }
}

export default Auth;

