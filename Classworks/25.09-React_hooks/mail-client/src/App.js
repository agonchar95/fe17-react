import React from 'react';
import './App.scss';
import Loader from "./components/Loader/Loader";
import Header from "./components/Header/Header";
import Inbox from "./components/Inbox/Inbox";
import Footer from "./components/Footer/Footer";
import Auth from './components/Auth/Auth'
import AppRoutes from "./routes/AppRoutes";

class App extends React.Component {
  state = {
    currentUser: localStorage.getItem("user"),
    title: "Welcome to our very first mail client",
    emails: [],
    isLoading: true
  };

  componentDidMount() {
    // axios.all([
    //   this.getEmails,
    //   this.getCurrentUser,
    // ]).then(() => {
    //   this.setState({isLoading: false})
    // })

    setTimeout(() => {
      // this.getCurrentUser();
      this.getEmails();
      this.setState({isLoading: false});
    }, 1000);
  }


  getEmails() {
    fetch('/inbox.json').then(r => r.json())
      .then(data => this.setState({emails: data}));
  };

  setCurrentUser(obj) {
    this.setState({currentUser: obj})
  }

  getCurrentUser() {
    fetch('/user.json').then(r => r.json())
      .then(data => this.setState({currentUser: data}));
  };

  render() {
    const {currentUser, emails, title, isLoading} = this.state;
    if (isLoading) {
      return <div className="App"><Loader/></div>;
    }

    return <div className="App">
      <Header/>
      <AppRoutes emails={emails} user={currentUser} setUser={u => this.setCurrentUser(u)}/>
      <Footer/>
    </div>

    // return currentUser
    //   ? <div className="App">
    //     <Inbox message={title} emails={emails}>
    //       This text will be in children
    //     </Inbox>
    //   </div>
    //   : <div className="App">
    //     <Auth setUser={(u) => this.setCurrentUser(u)}/>
    //   </div>;
  }
}

export default App;
