import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import SideBar from "../components/SideBar/SideBar";
import Inbox from "../components/Inbox/Inbox";
import Auth from "../components/Auth/Auth";

const AppRoutes = ({user, emails, setUser}) => {
  const isAuth = !!user;

  return (
    <>
      <ProtectedRoute authenticated={isAuth} path={'/'} component={SideBar}/>
      <Switch>
        <ProtectedRoute authenticated={isAuth}
                        path={'/inbox'}
                        render={(routeProps) => <Inbox emails={emails} message={"Yah! You did this!!! Go to sleep"} {...routeProps}>This text will be in children</Inbox>}/>

        <Route path={'/login'} render={() => isAuth ? <Redirect to='/'/> : <Auth setUser={setUser}/>}/>
      </Switch>
    </>
  );
};

export default AppRoutes;

const ProtectedRoute = ({authenticated, ...props}) => authenticated ? <Route {...props} /> : <Redirect to='/login' />;