import React, {Component} from 'react';

class Auth extends Component {
  name = React.createRef();
  password = React.createRef();

  handleSubmit (e) {
    e.preventDefault();
    const {setUser} = this.props;
    const {current:name} = this.name;
    const {current:password} = this.password;
    const user = {
      name: name.value,
      password: password.value
    };

    setUser(user);
    localStorage.setItem("user", JSON.stringify(user))
    // localStorage.setItem("user", JSON.stringify({
    //   name: name.value,
    //   password: password.value
    // }))
  }

  render() {
    return (
      <form onSubmit={(e) => this.handleSubmit(e)}>
        <input ref={this.name} type="text" minLength={3} required={true} placeholder={'Enter name'}/>
        <input ref={this.password} type="password" minLength={3} required={true} placeholder={'Enter password'}/>
        <input type="submit" value={"You don't want this, aren't you?"}/>
      </form>
    );
  }
}

export default Auth;