import React, { PureComponent } from 'react';
import './Inbox.scss';
import EmailCards from '../EmailCards/EmailCards';

class Inbox extends PureComponent {
  render() {
    const { message, emails, children } = this.props;
    
    return (
      <div className='app-body'>
        <h2 className='app-body__title'>
          Inbox
        </h2>
        <h2 className="app-body__message">
          {message}
        </h2>
        <p>{children}</p>
        <EmailCards emails={emails} />
      </div>
    )
  }
}

export default Inbox;