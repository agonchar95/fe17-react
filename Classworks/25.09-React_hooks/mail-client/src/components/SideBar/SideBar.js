import React, {Component} from 'react';
import {NavLink} from "react-router-dom";
import './SideBar.scss'

class SideBar extends Component {
  render() {
    return (
      <div>
        <NavLink to={'/'} exact={true} activeStyle={{color: 'coral'}} className={'nav-link'}>Home</NavLink>
        <NavLink to={'/inbox'} activeStyle={{color: 'coral'}} className={'nav-link'}>Go to inbox</NavLink>
      </div>
    );
  }
}

export default SideBar;