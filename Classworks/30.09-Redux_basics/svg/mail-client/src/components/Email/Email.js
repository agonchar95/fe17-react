import React from 'react';
import {Link, withRouter} from "react-router-dom";
import './Email.scss';

const Email = (props) => {
  const {topic, id, text, showFull, history} = props;

  const goBack = () => {
    history.goBack()
  }

  return (
    <div className='email'>
      <div>
        <Link className='email__link' to={`/emails/${id}`}>
          {topic}
        </Link>
      </div>
      {showFull && <div>{text}</div>}
      {showFull && (
        <div>
          <button onClick={goBack}>Go back</button>
        </div>
      )}
      {showFull && (
        <div>
          <Link to={`/emails/${id - 1}`}>Previous</Link>
          <Link to={`/emails/${id + 1}`}>Next</Link>
        </div>
      )}
    </div>
  );
}

export default withRouter(Email);