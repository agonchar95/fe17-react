import React, {useRef} from 'react';

const Auth = (props) => {
  const nameRef = useRef(null);
  const passwordRef = useRef(null);

  const handleSubmit = (e) => {
    e.preventDefault();
    const {setUser} = props;

    const user = {
      name: nameRef.current.value,
      password: passwordRef.current.value
    };

    setUser(user);
    localStorage.setItem("user", JSON.stringify(user))
  }

  return (
    <form onSubmit={handleSubmit}>
      <input ref={nameRef} type="text" minLength={3} required={true} placeholder={'Enter name'}/>
      <input ref={passwordRef} type="password" minLength={3} required={true} placeholder={'Enter password'}/>
      <input type="submit" value={"You don't want this, aren't you?"}/>
    </form>
  );
}

export default Auth;