import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import SideBar from "../components/SideBar/SideBar";
import Inbox from "../components/Inbox/Inbox";
import Auth from "../components/Auth/Auth";
import Page404 from "../pages/Page404/Page404";
import OneEmail from "../pages/OneEmail/OneEmail";

const AppRoutes = ({user, emails, setUser}) => {
    const isAuth = !!user;

    return (
        <>
            <ProtectedRoute authenticated={isAuth} path='/' component={SideBar}/>
            <Switch>
                <ProtectedRoute exact path='/' render={() => <Redirect to='/inbox' />} authenticated={isAuth} />
                <ProtectedRoute
                    authenticated={isAuth}
                    path='/inbox'
                    exact
                    render={(routeProps) => <Inbox emails={emails} {...routeProps} />}
                />
                <ProtectedRoute exact path='/emails/:emailId' component={OneEmail} authenticated={isAuth} />

                <Route exact path='/login' render={() => isAuth ? <Redirect to='/'/> : <Auth setUser={setUser}/>}/>
                <Route path='*' component={Page404}/>
            </Switch>
        </>
    );
};

export default AppRoutes;

const ProtectedRoute = ({authenticated, ...props}) => authenticated ? <Route {...props} /> : <Redirect to='/login'/>;