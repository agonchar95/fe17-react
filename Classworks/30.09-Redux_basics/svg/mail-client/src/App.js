import React, {useEffect, useState} from 'react';
import './App.scss';
import Loader from "./components/Loader/Loader";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import AppRoutes from "./routes/AppRoutes";
import axios from 'axios';

const App = () => {
  const [currentUser, setCurrentUser] = useState(localStorage.getItem("user"));
  const [emails, setEmails] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const getEmails = () => {
    return axios('/api/emails')
      .then(res => setEmails(res.data))
  };

  // const getCurrentUser = () => {
  //   return axios('/user.json')
  //     .then(res => setCurrentUser(res.data));
  // };

  useEffect(() => {
    // componentDidMount and componentDidUpdate
    setTimeout(() => {
      getEmails()
        .then(() => setIsLoading(false))
    }, 1000);

    return () => {
      // componentWillUnmount
    }
  }, [])

  if (isLoading) {
    return <div className="App"><Loader/></div>;
  }

  return <div className="App">
    <Header/>
    <AppRoutes emails={emails} user={currentUser} setUser={setCurrentUser}/>
    <Footer/>
  </div>
}

// axios.all([
//   this.getEmails,
//   this.getCurrentUser,
// ]).then(() => {
//   this.setState({isLoading: false})
// })

export default App;
