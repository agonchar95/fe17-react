import React, {Component} from 'react';
import './counter.css'

class Counter extends Component {
  state = {
    value: 0
  }
  counter(){
    this.setState({value: this.state.value + 1})
  }
  render() {
    return (
      <div className={'gogi'}>
        <p className="count-value">{this.state.value}</p>
        <button className='clicker' onClick={() => this.counter()}>Click me!</button>
      </div>
    );
  }
}


export default Counter;