import React from 'react';
import './App.scss';
import Loader from "./components/Loader/Loader";
import Header from "./components/Header/Header";
import Inbox from "./components/Inbox/Inbox";
import Footer from "./components/Footer/Footer";

class App extends React.Component {
  state = {
    currentUser: null,
    title: "Welcome to our very first mail client",
    emails: [],
    isLoading: true
  }
  
  componentDidMount() {
    // axios.all([
    //   this.getEmails,
    //   this.getState,
    // ]).then(() => {
    //   this.setState({isLoading: false})
    // })
    
    setTimeout(() => {
      this.getCurrentUser();
      this.getEmails();
      this.setState({isLoading: false});
    }, 1000);
  }
  
  
  getEmails = () => {
    fetch('./inbox.json').then(r => r.json())
      .then(data => this.setState({emails: data}));
  }
  
  getCurrentUser = () => {
    fetch('./user.json').then(r => r.json())
      .then(data => this.setState({currentUser: data}));
  }
  
  render() {
    const {currentUser, emails, title, isLoading} = this.state;
    
    return <div className="App">
      {
        isLoading
          ? <Loader/>
          : <>
            <Header user={currentUser}/>
            <Inbox message={title} emails={emails}>
              This text will be in children
            </Inbox>
            <Footer/>
          </>
      }
    </div>
  }
}

export default App;
