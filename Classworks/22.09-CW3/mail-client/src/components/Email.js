import React, {Component} from 'react';

class Email extends Component {
  render() {
    const {topic} = this.props;
    return (
      <div>{topic}</div>
    );
  }
}

export default Email;