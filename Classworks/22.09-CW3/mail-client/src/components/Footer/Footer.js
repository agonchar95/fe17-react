import React, {Component} from 'react';

class Footer extends Component {
  render() {
    return (
      <div className="container" style={{fontSize: '10px', textAlign: 'right'}}>
        hush...all rights have been stolen from other developers, but they don't know anything about it
      </div>
    );
  }
}

export default Footer;