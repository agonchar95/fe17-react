import React, {Component} from 'react';
import Email from "../Email";

class EmailCards extends Component {
  render() {
    const {emails} = this.props;
    const emailItems = emails.filter(email => !email.hidden).map((email, i) =>  <Email {...email} key={i}/>)
    return (
      <div>
        {emailItems}
      </div>
    );
  }
}

export default EmailCards;