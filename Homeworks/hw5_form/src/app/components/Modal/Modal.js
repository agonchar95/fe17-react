import './Modal.scss'
import React from 'react'

const Modal = (props) => {
  const {header, closeButton, text, actions, closeFunc} = props
  return (
    <div className="modal__wrapper" onClick={(event) => {
      if (event.target === event.currentTarget) {
        closeFunc()
      }
    }
    }>
      <div className={'modal'}>
        <div className="modal__header">
          <h4>{header}</h4>
          {closeButton && <p className={'modal__close-btn'} onClick={closeFunc}></p>}
        </div>
        <p className="modal__content">
          {text}
        </p>
        <div className="modal__actions">
          {actions}
        </div>
      </div>
    </div>
  );
}

export default Modal;