import React, {useEffect, useState} from 'react'
import './Shop.scss'
import Item from '../../app/components/Item/Item'
import {useSelector} from "react-redux";

const Shop = (props) => {
  const [favItems, setFavItems] = useState([])
  const items = useSelector((store) => store.items)

  useEffect(() => {
    setFavItems(JSON.parse(localStorage.getItem('favourites')) || [])
  }, [])

  const itemsToRender = items.map(el => <Item key={el.art} item={el} />)
  return (
    <div className={'shop-wrapper'}>
      {itemsToRender}
    </div>
  );
}

export default Shop;