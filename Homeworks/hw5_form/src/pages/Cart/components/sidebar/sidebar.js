import './sidebar.scss'
import React, {useState} from 'react';
import ConfirmationForm from "../confirmationForm/confirmationForm";

const Sidebar = () => {
  const [sideBar, toggleSidebar] = useState(false)

  const handleSidebar = function () {
    toggleSidebar(!sideBar)
  }

  return (
    <div className={sideBar ? 'sidebar sidebar--active' : 'sidebar sidebar--passive'}>
      <div className="sidebar__purchase" onClick={() => {
        handleSidebar()
      }}>
        <p >Purchase</p>
      </div>
        <div className="form" ><ConfirmationForm/></div>
    </div>
  );
};

export default Sidebar;