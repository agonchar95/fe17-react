import React from 'react';
import {Switch, Route} from 'react-router-dom'
import Shop from "../pages/Shop/Shop";
import Cart from "../pages/Cart/Cart";
import Favourites from "../pages/Favourites/Favourites";

const AppRoute = () => {
  return (
    <Switch>
      <Route exact path='/' component={Shop} />
      <Route exact path='/cart' component={Cart} />
      <Route exact path='/favourites' component={Favourites} />
    </Switch>
  );
};

export default AppRoute;