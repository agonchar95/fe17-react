import Button from "../../app/components/Button/Button";
import Modal from "../../app/components/Modal/Modal";
import React from "react";
import {addToCart, removeFromCart} from "./cartActions";

export const OPEN_MODAL = 'OPEN_MODAL'
export const OPEN_DELETE_MODAL = 'OPEN_DELETE_MODAL'
export const CLOSE_MODAL = 'CLOSE_MODAL'

export const openModal = (item) => (dispatch) => {
  dispatch({
    type: OPEN_MODAL,
    payload: <Modal
      header={'Are you sure?'}
      text={'Once you buy it, you will be unable to refund)'}
      closeFunc={() => dispatch(closeModal())}
      actions={
        <>
          <Button text={'Ok'} backgroundColor={'lightgreen'} onClick={() => {
            dispatch(addToCart(item))
            dispatch(closeModal())
          }}/>
          <Button text={'Cancel'} backgroundColor={'lightgreen'} onClick={() => dispatch(closeModal())}/>
        </>
      }
    />
  })
}

export const openDeleteModal = (item) => (dispatch) => {
  dispatch({
    type: OPEN_DELETE_MODAL,
    payload: <Modal
      header={'Are you sure?'}
      text={'Once you delete it, u will be cursed)'}
      closeFunc={() => dispatch(closeModal())}
      actions={
        <>
          <Button text={'Ok'} backgroundColor={'lightgreen'} onClick={() => {
            dispatch(removeFromCart(item))
            dispatch(closeModal())
          }}/>
          <Button text={'Cancel'} backgroundColor={'lightgreen'} onClick={() => dispatch(closeModal())}/>
        </>
      }
    />
  })
}

export const closeModal = () => {
  return {
    type: CLOSE_MODAL
  }
}