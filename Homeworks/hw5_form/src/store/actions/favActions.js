export const FAV_LOAD = 'FAV_LOAD'
export const ADD_TO_FAV = 'ADD_TO_FAV'
export const REMOVE_FROM_FAV = 'REMOVE_FROM_FAV'

export const favLoad = () => {
  return {
    type: FAV_LOAD,
    payload: JSON.parse(localStorage.getItem('favourites')) || []
  }
}

export const addToFav = (item) => {
  const favourites = JSON.parse(localStorage.getItem('favourites')) || []
  favourites.push(item)
  localStorage.setItem('favourites', JSON.stringify(favourites))
  return {
    type: ADD_TO_FAV,
    payload: favourites
  }
}

export const removeFromFav = (item) => {
  const favourites = JSON.parse(localStorage.getItem('favourites')) || []
  const favsArticles = favourites.map(el => el.art)
  favourites.splice(favsArticles.indexOf(item.art), 1)
  localStorage.setItem('favourites', JSON.stringify(favourites))
  return {
    type: REMOVE_FROM_FAV,
    payload: favourites
  }
}