import './Cart.scss'
import React, {useEffect} from 'react';
import Item from "../../app/components/Item/Item";
import {useDispatch, useSelector} from "react-redux";
import {loadCart} from "../../store/actions/cartActions";

const Cart = () => {
  const cartItems = useSelector((store) => store.cart)
  const itemsToRender = cartItems.map(el => <Item key={el.art} item={el} isCart={true} />)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(loadCart())
  }, [])

  return (
    <div className={'cart'}>
      {itemsToRender.length === 0 ?
        <p className={'cart-info'}>No Items was added</p>
        :
        <div className={'shop-wrapper'}>{itemsToRender}</div>}
    </div>
  );
};

export default Cart;