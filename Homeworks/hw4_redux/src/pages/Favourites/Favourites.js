import './Favourites.scss'
import React, {useEffect, useState} from 'react';
import Item from "../../app/components/Item/Item";
import {useDispatch, useSelector} from "react-redux";
import {favLoad} from "../../store/actions/favActions";

const Favourites = () => {
  const favItems = useSelector(store => store.favs)
  const itemsToRender = favItems.map(el => <Item key={el.art} item={el} />)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(favLoad())
  }, [])

  return (
    <div className={'favs'}>
      {itemsToRender.length === 0 ?
        <p className={'favs-info'}>No Items was added</p>
        :
        <div className={'shop-wrapper'}>{itemsToRender}</div>}
    </div>
  );
};

export default Favourites;