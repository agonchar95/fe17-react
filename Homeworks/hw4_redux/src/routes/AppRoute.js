import React from 'react';
import {Switch, Route} from 'react-router-dom'
import Shop from "../pages/Shop/Shop";
import Cart from "../pages/Cart/Cart";
import Favourites from "../pages/Favourites/Favourites";

const AppRoute = () => {
  return (
    <Switch>
      <Route exact path='/' render={() => <Shop/>} />
      <Route exact path='/cart' render={() => <Cart />} />
      <Route exact path='/favourites' render={() => <Favourites />} />
    </Switch>
  );
};

export default AppRoute;