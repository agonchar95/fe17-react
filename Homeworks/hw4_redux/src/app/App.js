import React, {useEffect} from 'react';
import './App.scss';
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import AppRoute from "../routes/AppRoute";
import {useDispatch, useSelector} from "react-redux";
import {loadItems} from "../store/actions/itemsActions";

const App = () => {
  const modal = useSelector((store) => store.modal)
  const dispatch = useDispatch()

  useEffect(() => {
    fetch('./items.json')
      .then(res => res.json())
      .then(data => dispatch(loadItems(data)))
  }, [])

  return (
    <div className={'container'}>
        <Header />
        <AppRoute/>
        {modal}
        <Footer />
    </div>
  );
};

export default App;