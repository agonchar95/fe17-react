import React from 'react';
import './Button.scss'

const Button = (props) => {
  const {backgroundColor, text, onClick} = props
  return (
    <button style={{backgroundColor: backgroundColor}} onClick={onClick} className={'btn'}>{text}</button>
  )
}

export default Button;