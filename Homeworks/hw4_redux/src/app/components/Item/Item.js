import React, {useEffect, useState} from 'react';
import './Item.scss'
import Button from "../Button/Button";
import {useDispatch, useSelector} from "react-redux";
import {openDeleteModal, openModal} from "../../../store/actions/modalActions";
import {addToFav, removeFromFav} from "../../../store/actions/favActions";

const Item = (props) => {
  const [buyAmount, setBuyAmount] = useState(0)
  const dispatch = useDispatch()
  const favouritesArticles = useSelector(store => store.favs).map(el => el.art)
  const {item, isCart} = props
  const isFav = favouritesArticles.includes(item.art)

  const handleFavs = function (item) {
    if (!(favouritesArticles.includes(item.art))) {
      dispatch(addToFav(item))
    } else {
      dispatch(removeFromFav(item))
    }
  }

  return (
    <div className={'item'}>
      {!isFav &&
      <i style={{color: isFav ? "gold" : 'white'}} className="fas fa-star fa-2x" onClick={() => handleFavs(item)}/>}
      {isFav && <i style={{color: isFav ? "gold" : 'white'}} className="fas fa-star fa-2x active-star"
                   onClick={() => handleFavs(item)}/>}
      <h3 className={'item-name'}>{item.name}</h3>
      <div className="item-image">
        <img src={item.src} alt={item.art}/>
      </div>
      <p className="item-art">Art.{item.art}</p>
      <p className="item-color">Color: {item.color}</p>
      <p className="item-price">Price: {item.price}</p>
      {isCart && <p className="item-quantity">Quantity: {JSON.parse(localStorage.getItem(item.art)) + 1}</p>}
      {!isCart && <Button text={'Add to Cart'} backgroundColor={'lightgreen'} onClick={() => {
        setBuyAmount(prevState => prevState + 1)
        localStorage.setItem(item.art, buyAmount)
        dispatch(openModal(item))
      }}/>}
      {isCart && <Button text={'Remove from Cart'} backgroundColor={'lightgreen'} onClick={() => {
        setBuyAmount(0)
        localStorage.setItem(item.art, buyAmount)
        dispatch(openDeleteModal(item))
      }}/>}
    </div>
  );
}

export default Item;