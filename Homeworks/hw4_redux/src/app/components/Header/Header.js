import React from 'react';
import './Header.scss'
import {Link} from "react-router-dom";

const Header = () => {
  return (
    <div className={'header-wrapper'}>
      <header className="header">
        <span className="header-slogan">FAN Shop</span>
        <Link to='/'><span className={'header-link'}>Main</span></Link>
        <Link to='/cart'><span className={'header-link'}>Cart</span></Link>
        <Link to='/favourites'><span className={'header-link'}>Favourites</span></Link>
      </header>
    </div>
  );
};

export default Header;