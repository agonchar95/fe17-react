export const ADD_TO_CART = 'ADD_TO_CART'
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART'
export const LOAD_CART = 'LOAD_CART'

export const addToCart = (item) => {
  const cart = JSON.parse(localStorage.getItem('cart')) || []
  const cartArticles = cart.map(el => el.art)
  if (!(cartArticles.includes(item.art))) {
    cart.push(item)
    localStorage.setItem('cart', JSON.stringify(cart))
  }
  return {
    type: ADD_TO_CART,
    payload: cart
  }
}

export const removeFromCart = (item) => {
  const cart = JSON.parse(localStorage.getItem('cart')) || []
  const cartArticles = cart.map(el => el.art)
  cart.splice(cartArticles.indexOf(item.art), 1)
  localStorage.setItem('cart', JSON.stringify(cart))
  return {
    type: REMOVE_FROM_CART,
    payload: cart
  }
}

export const loadCart = () => {
  return {
    type: LOAD_CART,
    payload: JSON.parse(localStorage.getItem('cart')) || []
  }
}