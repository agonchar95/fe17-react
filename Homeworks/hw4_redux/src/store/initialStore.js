const initialStore = {
  items: [],
  modal: null,
  cart: [],
  favs: []
}

export default initialStore