import React, {Component} from 'react';
import './Shop.scss'
import Item from "../Item/Item";

class Shop extends Component {
  render() {
    const {items, modal} = this.props
    const itemsToRender = items.map(el =>  <Item key={el.art} item={el} modal={modal}/>)
    return (
      <div className={'shop-wrapper'}>
        {itemsToRender}
      </div>
    );
  }
}

export default Shop;