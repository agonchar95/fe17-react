import React, {Component} from 'react';
import './Header.scss'

class Header extends Component {
  render() {
    return (
      <div className={'header-wrapper'}>
        <header className="header">
          <p className="header-slogan">Welcome To The FAN Shop</p>
        </header>
      </div>
    );
  }
}

export default Header;