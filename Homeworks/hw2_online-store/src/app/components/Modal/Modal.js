import './Modal.scss'
import React, {Component} from 'react'
import PropTypes from 'prop-types'
import Button from "../Button/Button";

class Modal extends Component {
  render() {
    const {header, closeButton, text, actions, closeFunc} = this.props
    return (
      <div className="modal__wrapper" onClick={(event) => {
        if (event.target === event.currentTarget) {
        closeFunc()}
    }
  }>
  <div className={'modal'}>
      <div className="modal__header">
      <h4>{header}</h4>
    {closeButton && <p className={'modal__close-btn'} onClick={closeFunc}> </p>}
      </div>
      <p className="modal__content">
      {text}
      </p>
      <div className="modal__actions">
      {actions}
      </div>
      </div>
      </div>
    );
  }
}

Modal.propTypes = {
  text: PropTypes.string.isRequired,
  header: PropTypes.string.isRequired,
  closeButton: PropTypes.bool.isRequired,
  actions: PropTypes.object.isRequired,
  closeFunc: PropTypes.func.isRequired
}

Modal.defaultProps = {
  text: 'Modal Text',
  header: 'Modal Title',
  closeButton: false,
  actions: <Button />,
  closeFunc: (event) => console.log(event.target)
}

export default Modal;