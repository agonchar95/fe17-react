import './Button.scss'
import PropTypes from 'prop-types'
import React, {Component} from 'react'

class Button extends Component {
  render() {
    const {backgroundColor, text, onClick} = this.props
    return (
      <button style={{backgroundColor: backgroundColor}} onClick={onClick} className={'btn'}>{text}</button>
    );
  }
}

Button.propTypes = {
  text: PropTypes.string.isRequired,
  backgroundColor: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
}

Button.defaultProps = {
  text: 'Try me',
  backgroundColor: 'aqua',
  onClick: (event) => console.log(event.target)
}

export default Button;