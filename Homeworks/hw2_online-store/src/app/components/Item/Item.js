import React, {Component} from 'react';
import './Item.scss'
import Button from "../Button/Button";

class Item extends Component {
  state = {
    isFav: false
  }

  addToFavs(item) {
    const favourites = JSON.parse(localStorage.getItem('favourites')) || []
    const favsArticles = favourites.map(el => el.art)
    if (!favsArticles.includes(item.art)) {
      favourites.push(item)
      localStorage.setItem('favourites', JSON.stringify(favourites))
    } else {
      favourites.splice(favsArticles.indexOf(item.art), 1)
      localStorage.setItem('favourites', JSON.stringify(favourites))
    }
    this.setState({isFav: !this.state.isFav})
  }

  componentDidMount() {
    const {art} = this.props.item
    const favItems = JSON.parse(localStorage.getItem('favourites')) || []
    this.setState({isFav: favItems.some(el => art === el.art)})
  }

  render() {
    const {item, modal} = this.props
    const isFav = this.state.isFav
    return (
      <div className={'item'}>
        <i style={{color: isFav ? "gold" : 'white'}} className="fas fa-star fa-2x" onClick={() => this.addToFavs(item)}></i>
        <h3 className={'item-name'}>{item.name}</h3>
        <div className="item-image">
          <img src={item.src} alt={item.art}/>
        </div>
        <p className="item-art">Art.{item.art}</p>
        <p className="item-color">Color: {item.color}</p>
        <p className="item-price">Price: {item.price}</p>
        <Button text={'Add to Cart'} backgroundColor={'lightgreen'} onClick={() => modal(item)}/>
      </div>
    );
  }
}

export default Item;