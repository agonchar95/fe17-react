import React, {Component} from 'react';
import './App.scss';
import Header from "./components/Header/Header";
import Shop from "./components/Shop/Shop";
import Footer from "./components/Footer/Footer";
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button";

class App extends Component {
  state = {
    modal: null,
    items: [],
    cart: []
  }

  componentDidMount() {
    fetch('./items.json')
      .then(res => res.json())
      .then(data => this.setState({
        items: data
      }))
  }



  setModal(item) {
    this.setState({
      modal:
        <Modal
          header={'Are you sure?'}
          text={'Once you buy it, you will be unable to refund)'}
          closeFunc={() => this.setState({modal: null})}
          actions={
            <>
              <Button text={'Ok'} backgroundColor={'lightgreen'} onClick={() => this.itemToCart(item)}/>
              <Button text={'Cancel'} backgroundColor={'lightgreen'} onClick={() => this.setState({modal: null})}/>
            </>
          }
        />
    })
  }

  itemToCart(item) {
    const {cart} = this.state
    cart.push(item)
    localStorage.setItem('cart', JSON.stringify(cart))
    this.setState({cart: cart, modal: null})

  }

  render() {
    const {items, modal} = this.state
    return (
      <div className={'container'}>
          <Header />
          <Shop items={items} modal={(item) => this.setModal(item)}/>
          {modal}
          <Footer />
      </div>
    );
  }
}

export default App;
