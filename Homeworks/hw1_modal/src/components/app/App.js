import Button from "../button/Button";
import './App.scss'
import Modal from "../modal/Modal";
import React, {Component} from 'react';

class App extends Component {
  state = {
    modal: null
  }

  setModal(modal) {
    switch (modal) {
      case 'first':
        this.setState({modal:
            <Modal
              header={'Do you want to delete this file? '}
              text={'Once you delete this file, it won’t be possible to undo this action.  Are you sure you want to delete it? '}
              closeFunc={() => this.setModal(null)}
              actions={
                <>
                  <Button text={'Ok'} backgroundColor={'orangered'} onClick={() => this.setModal(null)} />
                  <Button text={'Cancel'} backgroundColor={'orangered'} onClick={() => this.setModal(null)} />
                </>
              }
            />
        })
        break
      case 'second':
        this.setState({modal:
            <Modal
              header={'Just simple modal '}
              text={'just simple modal demo text'}
              closeButton={false}
              closeFunc={() => this.setModal(null)}
              actions={
                <>
                  <Button text={'Ok'} backgroundColor={'orangered'} onClick={() => this.setModal(null)} />
                </>
              }
            />
        })
        break
      default:
        this.setState({
          modal: null
        })
    }
  }

  render() {
    const {modal} = this.state
    return (
      <div className="App">
        <div className={'button-wrapper'}>
          <Button text={'Open first modal'} backgroundColor={'rebeccapurple'} onClick={() => this.setModal('first')} />
          <Button text={'Open second modal'} backgroundColor={'black'} onClick={() => this.setModal('second')} />
        </div>
        {modal}
      </div>
    );
  }
}

export default App;