import './Button.scss'
import PropTypes from 'prop-types'
import React, {Component} from 'react'

class Button extends Component {
  render() {
    const {backgroundColor, text, onClick} = this.props
    return (
      <button style={{backgroundColor: backgroundColor}} onClick={onClick} className={'modal-btn'}>{text}</button>
    );
  }
}

Button.propTypes = {
  text: PropTypes.string,
  backgroundColor: PropTypes.string,
  onClick: PropTypes.func
}

Button.defaultProps = {
  text: 'Try me',
  backgroundColor: 'aqua'
}

export default Button;