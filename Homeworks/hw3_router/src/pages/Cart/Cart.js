import './Cart.scss'
import React, {useEffect, useState} from 'react';
import Item from "../../app/components/Item/Item";

const Cart = (props) => {
  const [favItems, setFavItems] = useState([])
  const [cartItems, setCartItems] = useState([])
  const {modal, closeModal} = props
  const itemsToRender = cartItems.map(el => <Item key={el.art} item={el} setFavItems={setFavItems} modal={modal} isCart={true} closeModal={closeModal}/>)

  useEffect(() => {
    setCartItems(JSON.parse(localStorage.getItem('cart')) || [])
    setFavItems(JSON.parse(localStorage.getItem('favourites')) || [])
  }, [(localStorage.getItem('cart'))])

  return (
    <div className={'cart'}>
      {itemsToRender.length === 0 ?
        <p className={'cart-info'}>No Items was added</p>
        :
        <div className={'shop-wrapper'}>{itemsToRender}</div>}
    </div>
  );
};

export default Cart;