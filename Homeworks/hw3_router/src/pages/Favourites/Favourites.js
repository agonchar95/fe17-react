import './Favourites.scss'
import React, {useEffect, useState} from 'react';
import Item from "../../app/components/Item/Item";

const Favourites = (props) => {
  const [favItems, setFavItems] = useState([])
  const {modal} = props
  const itemsToRender = favItems.map(el => <Item key={el.art} setFavItems={setFavItems} item={el} modal={modal} />)

  useEffect(() => {
    setFavItems(JSON.parse(localStorage.getItem('favourites')) || [])

  }, [localStorage.getItem('favourites')])

  return (
    <div className={'favs'}>
      {itemsToRender.length === 0 ?
        <p className={'favs-info'}>No Items was added</p>
        :
        <div className={'shop-wrapper'}>{itemsToRender}</div>}
    </div>
  );
};

export default Favourites;