import React from 'react'
import './Footer.scss'

const Footer = () => {
  return (
    <footer className={'footer'}>
      <p style={{fontSize: '15px'}}>All Rights Reserved</p>
      <p style={{fontSize: '10px'}}>2020, Ukraine, DanIT&trade;</p>
    </footer>
  );
};

export default Footer;