import React, {useEffect, useState} from 'react';
import './Item.scss'
import Button from "../Button/Button";

const Item = (props) => {
  const [isFav, setIsFav] = useState(false)
  const [buyAmount, setBuyAmount] = useState(0)

  const addToFavs = function (item) {
    const {setFavItems} = props
    let favourites = JSON.parse(localStorage.getItem('favourites')) || []
    const favsArticles = favourites.map(el => el.art)
    if (!(favsArticles.includes(item.art))) {
      favourites.push(item)
      localStorage.setItem('favourites', JSON.stringify(favourites))
      setFavItems(favourites)
    } else {
      favourites.splice(favsArticles.indexOf(item.art), 1)
      localStorage.setItem('favourites', JSON.stringify(favourites))
      setFavItems(favourites)
    }
    setIsFav(!isFav)
  }

  useEffect(() => {
    const {art} = item
    const favItems = JSON.parse(localStorage.getItem('favourites')) || []
    setIsFav(favItems.some(el => art === el.art))
  }, [])

  const {item, modal, isCart, closeModal} = props
    return (
      <div className={'item'}>
        {!isFav && <i style={{color: isFav ? "gold" : 'white'}} className="fas fa-star fa-2x" onClick={() => addToFavs(item)}></i>}
        {isFav && <i style={{color: isFav ? "gold" : 'white'}} className="fas fa-star fa-2x active-star" onClick={() => addToFavs(item)}></i>}
        <h3 className={'item-name'}>{item.name}</h3>
        <div className="item-image">
          <img src={item.src} alt={item.art}/>
        </div>
        <p className="item-art">Art.{item.art}</p>
        <p className="item-color">Color: {item.color}</p>
        <p className="item-price">Price: {item.price}</p>
        {isCart && <p className="item-quantity">Quantity: {JSON.parse(localStorage.getItem(item.art))+1}</p>}
        {!isCart && <Button text={'Add to Cart'} backgroundColor={'lightgreen'} onClick={() => {
          setBuyAmount(prevState => prevState + 1)
          localStorage.setItem(item.art, buyAmount)
          modal(item)
        }}/>}
        {isCart && <Button text={'Remove from Cart'} backgroundColor={'lightgreen'} onClick={() => {
          setBuyAmount(0)
          localStorage.setItem(item.art, buyAmount)
          closeModal(item)
        }}/>}
      </div>
    );
  }

export default Item;