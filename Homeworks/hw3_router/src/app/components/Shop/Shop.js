import React, {useEffect, useState} from 'react'
import './Shop.scss'
import Item from "../Item/Item"

const Shop = (props) => {
  const [favItems, setFavItems] = useState([])

  useEffect(() => {
    setFavItems(JSON.parse(localStorage.getItem('favourites')) || [])
  }, [])

  const {items, modal} = props
  const itemsToRender = items.map(el => <Item key={el.art} item={el} modal={modal} setFavItems={setFavItems} />)
  return (
    <div className={'shop-wrapper'}>
      {itemsToRender}
    </div>
  );
}

export default Shop;