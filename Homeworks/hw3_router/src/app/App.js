import React, {useEffect, useState} from 'react';
import './App.scss';
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button";
import AppRoute from "../routes/AppRoute";

const App = () => {
  const [modal, setModal] = useState(null)
  const [items, setItems] = useState([])

  useEffect(() => {
    fetch('./items.json')
      .then(res => res.json())
      .then(data => setItems(data))
  }, [])

  const openModal = function (item) {
    setModal(
        <Modal
          header={'Are you sure?'}
          text={'Once you buy it, you will be unable to refund)'}
          closeFunc={() => setModal(null)}
          actions={
            <>
              <Button text={'Ok'} backgroundColor={'lightgreen'} onClick={() => itemToCart(item)}/>
              <Button text={'Cancel'} backgroundColor={'lightgreen'} onClick={() => setModal(null)}/>
            </>
          }
        />
    )
  }
  const openDeleteModal = function (item) {
    setModal(
      <Modal
        header={'Are you sure?'}
        text={'Once you delete it, u will be cursed)'}
        closeFunc={() => setModal(null)}
        actions={
          <>
            <Button text={'Ok'} backgroundColor={'lightgreen'} onClick={() => itemFromCart(item)}/>
            <Button text={'Cancel'} backgroundColor={'lightgreen'} onClick={() => setModal(null)}/>
          </>
        }
      />
    )
  }

  const itemToCart = function(item) {
    const cart = JSON.parse(localStorage.getItem('cart')) || []
    const cartArticles = cart.map(el => el.art)
    if (!(cartArticles.includes(item.art))) {
      cart.push(item)
      localStorage.setItem('cart', JSON.stringify(cart))
    }
    setModal(null)
  }

  const itemFromCart = function(item) {
    const cart = JSON.parse(localStorage.getItem('cart')) || []
    const cartArticles = cart.map(el => el.art)
    cart.splice(cartArticles.indexOf(item.art), 1)
    localStorage.setItem('cart', JSON.stringify(cart))
    setModal(null)
  }

  return (
    <div className={'container'}>
        <Header />
        <AppRoute items={items} openModal={openModal} openDeleteModal={openDeleteModal} />
        {modal}
        <Footer />
    </div>
  );
};

export default App;
