import React from 'react';
import {Switch, Route} from 'react-router-dom'
import Shop from "../app/components/Shop/Shop";
import Cart from "../pages/Cart/Cart";
import Favourites from "../pages/Favourites/Favourites";

const AppRoute = (props) => {
  const {items, openModal, openDeleteModal} = props
  return (
    <Switch>
      <Route exact path='/' render={() => <Shop items={items} modal={(item) => openModal(item)} />} />
      <Route exact path='/cart' render={() => <Cart modal={(item) => openModal(item)} closeModal={(item) => openDeleteModal(item)}/>} />
      <Route exact path='/favourites' render={() => <Favourites items={items} modal={(item) => openModal(item)}/>} />
    </Switch>
  );
};

export default AppRoute;