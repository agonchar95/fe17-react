import initialStore from "../initialStore";
import {CLOSE_MODAL, OPEN_DELETE_MODAL, OPEN_MODAL} from "../actions/modalActions";

const modalReducer = (modal = initialStore.modal, action) => {
  switch(action.type) {
    case OPEN_MODAL: return action.payload
    case OPEN_DELETE_MODAL: return action.payload
    case CLOSE_MODAL: return null
    default: return modal
  }
}

export default modalReducer