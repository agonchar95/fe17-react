import {combineReducers} from "redux";
import itemsReducer from "./itemsReducer";
import modalReducer from "./modalReducer";
import cartReducer from "./cartReducer";
import favsReducer from "./favsReducer";

const rootReducer = combineReducers({
  items: itemsReducer,
  modal: modalReducer,
  cart: cartReducer,
  favs: favsReducer
})

export default rootReducer