import initialStore from "../initialStore";
import {ADD_TO_FAV, FAV_LOAD, REMOVE_FROM_FAV} from "../actions/favActions";

const favsReducer = (favs = initialStore.favs, action) => {
  switch (action.type) {
    case FAV_LOAD: return action.payload
    case ADD_TO_FAV: return action.payload
    case REMOVE_FROM_FAV: return action.payload
    default: return favs
  }
}

export default favsReducer