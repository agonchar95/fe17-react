import initialStore from "../initialStore";
import {LOAD_ITEMS} from "../actions/itemsActions";

const itemsReducer = (items = initialStore.items, action) => {
  switch (action.type) {
    case LOAD_ITEMS: return action.payload
    default: return items
  }
}

export default itemsReducer