import initialStore from "../initialStore";
import {ADD_TO_CART, CLEAN_CART, LOAD_CART, REMOVE_FROM_CART} from "../actions/cartActions";

const cartReducer = (cart = initialStore.cart, action) => {
  switch(action.type) {
    case ADD_TO_CART: return action.payload
    case REMOVE_FROM_CART: return action.payload
    case LOAD_CART: return action.payload
    case CLEAN_CART: return []
    default: return cart
  }
}

export default cartReducer