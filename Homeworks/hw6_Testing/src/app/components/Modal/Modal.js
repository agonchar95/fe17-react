import './Modal.scss'
import React from 'react'

const Modal = (props) => {
  const {header, closeButton, text, actions, closeFunc} = props
  return (
    <div data-testid='testModalWrapper' className="modal__wrapper" onClick={(event) => {
      if (event.target === event.currentTarget) {
        closeFunc()
      }
    }
    }>
      <div data-testid = 'testModal' className={'modal'}>
        <div className="modal__header">
          <h4 data-testid = 'testModalHeader' >{header}</h4>
          {closeButton && <p className={'modal__close-btn'} onClick={closeFunc}/>}
        </div>
        <p data-testid = 'testModalText' className="modal__content">
          {text}
        </p>
        <div data-testid='testModalActions' className="modal__actions">
          {actions}
        </div>
      </div>
    </div>
  );
}

export default Modal;