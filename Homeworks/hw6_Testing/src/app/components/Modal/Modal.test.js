import React from 'react'
import {fireEvent, render} from '@testing-library/react'
import Button from "../Button/Button";
import Modal from "./Modal";

describe('Modal component tests', function () {
  test('Test if render', function () {
    const {getByTestId} = render(<Modal />)
    expect(getByTestId('testModal')).not.toBe(null)
  })

  test('Test  modal header content', function () {
    const {getByTestId} = render(<Modal header={'Test me!'} />)
    expect(getByTestId('testModalHeader').textContent).toBe('Test me!')
  })

  test('Test modal header tag name', function () {
    const {getByTestId} = render(<Modal header={'ahahah'} />)
    expect(getByTestId('testModalHeader').tagName).toBe('H4')
  })

  test('Test modal header text content', function () {
    const {getByTestId} = render(<Modal text={'Test Text supahotfire'} />)
    expect(getByTestId('testModalText').textContent).toBe('Test Text supahotfire')
  })

  test('Test modal wrapper click', function () {
    const someFunc = jest.fn()
    const {getByTestId} = render(<Modal closeFunc={someFunc} />)
    fireEvent.click(getByTestId('testModalWrapper'))
    fireEvent.click(getByTestId('testModalWrapper'))
    expect(someFunc).toBeCalledTimes(2)
  })

  test('Test modal actions', function () {
    const actions = <Button />
    const {getByTestId} = render(<Modal actions={actions} />)
    expect(getByTestId('testModalActions').children).toHaveLength(1)
  })
})
