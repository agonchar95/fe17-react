import React from 'react'
import {fireEvent, render} from '@testing-library/react'
import Button from "./Button";


describe('Button component tests', function () {
  test('Test if render', function () {
  const {getByTestId} = render(<Button />)
  expect(getByTestId('testButton')).not.toBe(null)
  })

  test('Test text content', function () {
  const {getByTestId} = render(<Button text={'Test me!'} />)
  expect(getByTestId('testButton').textContent).toBe('Test me!')
  })

  test('Test background color', function () {
  const {getByTestId} = render(<Button backgroundColor={'teal'} />)
  expect(getByTestId('testButton').style.backgroundColor).toBe('teal')
  })

  test('Test if onclick', function () {
    const someFunc = jest.fn()
    const {getByTestId} = render(<Button onClick={someFunc} />)
    fireEvent.click(getByTestId('testButton'))
    expect(someFunc).toBeCalledTimes(1)
  })
})