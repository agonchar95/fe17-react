import React from 'react';
import './confirmationForm.scss'
import {useDispatch, useSelector} from "react-redux";
import {object, string} from "yup";
import {ErrorMessage, Field, Form, Formik} from "formik";
import {cleanCart} from "../../../../store/actions/cartActions";

const ConfirmationForm = () => {
  const dispatch = useDispatch()
  const cart = useSelector(store => store.cart)

  const initialValues = {
    name: '',
    surname: '',
    age: '',
    address: '',
    phone: ''
  }

  const submitHandle = (values) => new Promise(res => {
    setTimeout(() => {
      dispatch(cleanCart())
      console.log({
        order: cart,
        userInfo: values
      })
      res()
    }, 500)
  })


  const validationSchema = object({
    name: string()
      .min(3)
      .max(20)
      .required(),
    surname: string()
      .min(3)
      .max(20)
      .required(),
    age: string()
      .min(1)
      .max(100)
      .required(),
    address: string()
      .min(5, 'Incorrect Address')
      .required(),
    phone: string()
      .min(9, 'Incorrect Phone')
      .max(12, 'Incorrect Phone')
      .required()
  })

  return (
    <Formik initialValues={initialValues} onSubmit={submitHandle} validationSchema={validationSchema}>
      {
        ({isSubmitting, isValid}) => <Form className='cart-form'>
          <Field className='cart-form__input' name='name' placeholder='Name'/>
          <ErrorMessage name='name' render={(msg) => <p className={'cart-form__error'}>{msg}</p>}/>
          <Field className='cart-form__input' name='surname' placeholder='Surname'/>
          <ErrorMessage name='surname' render={(msg) => <p className={'cart-form__error'}>{msg}</p>}/>
          <Field className='cart-form__input' name='age' placeholder='Age' type={'number'}/>
          <ErrorMessage name='age' render={(msg) => <p className={'cart-form__error'}>{msg}</p>}/>
          <Field className='cart-form__input' name='address' placeholder='Address'/>
          <ErrorMessage name='address' render={(msg) => <p className={'cart-form__error'}>{msg}</p>}/>
          <Field className='cart-form__input' name='phone' placeholder='Phone Number'/>
          <ErrorMessage name='phone' render={(msg) => <p className={'cart-form__error'}>{msg}</p>}/>
          <button className='cart-form__submit'
                  type='submit'
                  disabled={isSubmitting || !isValid}
          >Checkout
          </button>
        </Form>
      }
    </Formik>
  );
};

export default ConfirmationForm;